#!/usr/bin/env python3

import yaml

def main(files):
    import os
    dir_path = os.path.dirname(os.path.realpath(__file__))
    fn = dir_path + "/../build/index.html"
    fh = open(fn,'w')
    print("<html><head><meta charset='utf-8'><title>All FossMoji Glyphs</title>", file=fh)
    print("<style> \
               @font-face{font-family:'FossMoji';src:url('file.ttf') format('truetype');} \
               div span{font-family:'FossMoji';} \
               div {display: inline-block; font-size: 50px;} \
               img {width: 50px;} \
           </style>", file=fh)
    print("</head><body>", file=fh)
    for file in files:
        if ".yaml" in file:
            glyph, extension = os.path.splitext(os.path.basename(file))
            object = yaml.load(open(file))
            code = "&#x" + glyph.replace("-", ";&#x") + ";"
            output = ("<div title=\"(%s) %s\">%s<span>%s</span><img src='./svg/%s.svg'/><img width='20' src='./svg-bw/%s.svg'/></div>"
                      % (glyph, object['name'], code, code, glyph, glyph))
            print(output, file=fh)
    print("</body></html>", file=fh)

if __name__ == "__main__":
    import sys
    main(sys.argv)
