"""
Effects to apply to SVGs.
Author: Zach DeCook
License: GPLv3 + FE
"""
from xml.etree import ElementTree

def rotate(xml, degrees):
    # The root should be an svg with width and height attributes.
    root = xml.getroot()
    width = float(root.attrib['width'])
    height = float(root.attrib['height'])
    pass
    all = root.findall('*')
    rotateString = "rotate(%f %f %f)" % (degrees, width/2, height/2)
    group = ElementTree.SubElement(root, 'g', {'transform': rotateString})
    for el in all:
        root.remove(el)
        group.append(el)
