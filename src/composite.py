#!/usr/bin/env python3
"""
Combine SVGs together, following particular instructions.
Author: Zach DeCook
License: GPLv3 + FE
"""
import os
import yaml
import sys
from xml.etree import ElementTree

import effects

def makeGlyph(file, dir_path):
    """
    Follow instructions in a yaml file to combine SVG files into another SVG file.
    """
    svgdir = dir_path + "/svg/"
    yamldir = dir_path + "/yaml/"
    outputdir = dir_path + "/../build/svg/"
    glyph, extension = os.path.splitext(os.path.basename(file))

    object = yaml.load(open(file))

    # TODO: Consider switching to lxml because it handles namespaces better
    # (https://stackoverflow.com/a/18340176).
    ElementTree.register_namespace('',"http://www.w3.org/2000/svg")

    try:
        base = ElementTree.parse(svgdir + object['base'])
    except FileNotFoundError:
        error("Could not find %s for %s (%s)." %(object['base'], glyph, object['name']))
        return
    root = base.getroot()
    for layer in object['layers']:
        svg = layer
        transformations = []
        if isinstance(layer, dict):
            for svg, transformations in layer.items():
                pass
        # Try to use the layer.
        try:
            tree = ElementTree.parse(svgdir + svg)

            for t in transformations:
                for effectName, effectParams in t.items():
                    # Try to apply the effect.
                    try:
                        # Call the effects function by string from the effects module we imported.
                        # The effect will modify the tree object, so there is no return value needed here.
                        getattr(effects, effectName)(tree, effectParams)
                    except AttributeError:
                        error("Effect %s not implemented for %s (%s)" % (method, glyph, object['name']))

            # Add layer's tags that aren't top level to the base svg.
            root.extend(tree.getroot().findall('*'))
        except FileNotFoundError:
            error("Could not find %s for %s (%s)." % (svg, glyph, object['name']))
    base.write(outputdir + glyph + ".svg")

def error(message):
    sys.stderr.write(message + "\n");

if __name__ == "__main__":
    import sys
    dir_path = os.path.dirname(os.path.realpath(__file__))
    for file in sys.argv:
        if ".yaml" in file:
            makeGlyph(file, dir_path)
