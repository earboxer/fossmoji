all: build/file.ttf build/index.html

build/svg/*.svg: src/svg/*/*/*.svg src/svg/*/*.svg src/svg/*.svg src/yaml/*/*/*.yaml src/*.py
	mkdir -p build/svg
	src/composite.py src/yaml/*/*/*.yaml

build/svg-bw/*.svg: build/svg/*.svg
    # Create the glyph versions by just removing the fill from the svgs.
	mkdir -p build/svg-bw
	cp build/svg/* build/svg-bw/
	# It can be in the style attribute
	sed -i 's/fill:[^:;]*;/fill:none;/g' build/svg-bw/*
	# Or as its own attribute.
	sed -i 's/fill="[^"]*"/fill="none"/g' build/svg-bw/*
	sed -i "s/fill='[^']*'/fill='none'/g" build/svg-bw/*

build/file.ttf: build/svg/*.svg build/svg-bw/*.svg scfbuild.yaml
	scfbuild -c scfbuild.yaml
	zip -ru build/source.zip src scfbuild.yaml Makefile LICENSE CONTRIBUTORS index.html

build/index.html: src/debug.py src/yaml/*/*/*.yaml
	src/debug.py src/yaml/*/*/*.yaml

.PHONY: clean
clean:
	rm -rf build
